package ship.contents;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by John on 27.12.2016.
 */
public class CargoCompartment extends Compartment {

    protected CargoItem cargoItem;

    private Set<CargoItem> cargoItemSet = new HashSet<>();

    public CargoCompartment(String UUID) {
        super(UUID);

    }

    public void setCargoItem(CargoItem cargoItem) {
        this.cargoItem = cargoItem;
    }

    public void addCarriable(){
        CargoItem cargoItem = new CargoItem(this.cargoItem.toString(), this.cargoItem.getProfit());
        cargoItemSet.add(cargoItem);
        System.out.println("Item " + cargoItem.toString() + " added to the Compartment with ID " + super.UUID);
    }

    @Override
    public void computeProfit() {
        profit = cargoItemSet.size()*cargoItem.getProfit();
    }

    @Override
    public double getProfit() {
        computeProfit();
        return profit;
    }

}
