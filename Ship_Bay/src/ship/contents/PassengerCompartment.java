package ship.contents;
import ship.constants.Constants;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by John on 27.12.2016.
 */
public class PassengerCompartment extends Compartment {


    public Set<Passenger> passengerSet = new HashSet<Passenger>() {

        @Override
        public boolean contains(Object o) {
            for (Passenger p : passengerSet) {
                if (p.toString() == o.toString()) {
                    return true;
                }
            }
            return false;

        }
    };

    public PassengerCompartment(String UUID) {
        super(UUID);
    }

    public void addPassenger(Passenger passenger) {
        if (passengerSet.size() < Constants.passengerCapacity) {
            if (passengerSet.contains(passenger)) {
                System.out.println("Passenger " + passenger.toString() + " already in the Compartment with ID " + super.UUID);
            } else {
                passengerSet.add(passenger);
                System.out.println("Passenger " + passenger.toString() + " successfully added to the Compartment with ID " + super.UUID);
            }
        } else {
            System.out.println("Passenger limit for compartment with ID " + super.UUID + " reached.");
        }
    }

    @Override
    public void computeProfit() {
        profit = Constants.ticketPrice * passengerSet.size();
    }

    @Override
    public double getProfit() {
        computeProfit();
        return profit;
    }

}

