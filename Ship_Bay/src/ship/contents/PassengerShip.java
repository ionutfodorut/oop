package ship.contents;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by John on 28.12.2016.
 */

public class PassengerShip extends Ship {

    private Set<PassengerCompartment> compartmentSet = new LinkedHashSet<PassengerCompartment>();

    public PassengerShip(String name) {
        super(name);
    }

    @Override
    public void addCompartment(Compartment compartment) {
        PassengerCompartment passengerCompartment = (PassengerCompartment) compartment;
        compartmentSet.add(passengerCompartment);
        System.out.println("Passenger compartment " + compartment.UUID + " added to ship " + name + " succesfully.");

    }

    public void computeProfit() {
        for (PassengerCompartment compartment: compartmentSet) {
            profit += compartment.getProfit();
        }
    }

    public double getProfit() {
        if (profit == 0 ) {
            computeProfit();
        }
        return profit;
    }

    public void shipSummary() {
        System.out.println (
                "Ship name: " + name + "; Number of compartments: " +
                        compartmentSet.size() + "; Profit: " + getProfit() + ";"
        );
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
