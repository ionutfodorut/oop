package ship.contents;

/**
 * Created by John on 27.12.2016.
 */
public class Passenger implements Carriable {

    private final String name;

    public Passenger(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
