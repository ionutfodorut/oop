package ship.contents;

/**
 * Created by John on 27.12.2016.
 */
public class CargoItem implements Carriable {

    private final String name;
    private final int profit;

    public CargoItem(String name, int profit) {
        this.name = name;
        this.profit = profit;
    }

    public String toString() {
        return name;
    }

    public int getProfit() {
        return profit;
    }

}
