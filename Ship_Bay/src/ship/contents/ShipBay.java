package ship.contents;

import org.omg.PortableServer.LIFESPAN_POLICY_ID;
import ship.contents.Ship;
import ship.sort.AlphabeticalSort;
import ship.sort.ProfitSort;

import java.util.*;

/**
 * Created by John on 20.12.2016.
 */
public class ShipBay {

    private Set<Ship> shipSet = new LinkedHashSet<Ship>();

    public void receiveShip(Ship ship) {
        if (shipSet.contains(ship))
            System.out.println("Ship " + ship.getName() + " is already in the Bay.");
        else {
            shipSet.add(ship);
            System.out.println("Ship " + ship.getName() + " received in the Bay");
        }
    }

    public void departShip(Ship ship) {
        if (shipSet.contains(ship)) {
            shipSet.remove(ship);
            System.out.println("Ship " + ship.getName() + " departed from the Bay.");
        }
        else
            System.out.println("Ship " + ship.getName() + " is not in the Bay.");
    }

    public void checkShip(Ship ship) {
        if (shipSet.contains(ship))
            System.out.println("Ship " + ship.getName() + " is in the Bay.");

        else
            System.out.println("Ship " + ship.getName() + " is not in the Bay.");
    }

    public void sortByName() {
        List<Ship> alphabeticalList = new ArrayList<Ship>(shipSet);
        Collections.sort(alphabeticalList, new AlphabeticalSort());
        for (int i = 0; i < alphabeticalList.size(); i++) {
            alphabeticalList.get(i).shipSummary();
        }
    }

    public void sortByProfit() {
        List<Ship> profitList = new ArrayList<Ship>(shipSet);
        Collections.sort(profitList, new ProfitSort());
        for (int i = 0; i < profitList.size(); i++) {
            profitList.get(i).shipSummary();
        }
     }

}
