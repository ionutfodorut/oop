package ship.contents;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by John on 28.12.2016.
 */

public class CargoShip extends Ship {

    private Set<CargoCompartment> compartmentSet = new LinkedHashSet<CargoCompartment>();

    public CargoShip(String name) {
        super(name);
    }

    @Override
    public void addCompartment(Compartment compartment) {
        CargoCompartment cargoCompartment = (CargoCompartment) compartment;
        compartmentSet.add(cargoCompartment);
        System.out.println("Cargo compartment " + compartment.UUID + " added to ship " + name + " succesfully.");
    }

    public void computeProfit() {
        for (CargoCompartment compartment: compartmentSet) {
            profit += compartment.getProfit();
        }
    }

    public double getProfit() {
        if (profit == 0 ) {
            computeProfit();
        }
        return profit;
    }

    public void shipSummary() {
        System.out.println (
                "Ship name: " + name + "; Number of compartments: " +
                        compartmentSet.size() + "; Profit: " + getProfit() + ";"
        );
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
