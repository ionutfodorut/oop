package ship.contents;

import ship.constants.Constants;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by John on 27.12.2016.
 */
public abstract class Ship implements Comparable {

    protected String name;

    protected double profit = 0;

    public String getName() {
        return name;
    }

    public Ship(String name) {
        this.name = name;
    }

    public abstract void addCompartment(Compartment compartment);

    public abstract void computeProfit();

    public abstract double getProfit();

    public abstract void shipSummary();

}
