package ship.contents;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by John on 20.12.2016.
 */

public abstract class Compartment {

    protected final String UUID;
    protected double profit = 0;

    public Compartment(String UUID) {
        this.UUID = UUID;
    }

    public abstract void computeProfit();

    @Override
    public String toString() {
        return UUID;
    }

    public abstract double getProfit();

}
