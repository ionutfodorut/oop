package ship.sort;

import ship.contents.Ship;

import java.util.Comparator;

/**
 * Created by John on 28.12.2016.
 */

public class ProfitSort implements Comparator<Ship> {
    @Override
    public int compare(Ship o1, Ship o2) {
        return ((o1.getProfit() < o1.getProfit()) ? -1 : ((o1.getProfit() > o2.getProfit()) ? 1 : 0));
    }
}
