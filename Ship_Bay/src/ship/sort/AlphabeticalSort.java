package ship.sort;

import ship.contents.Ship;

import java.util.Comparator;

/**
 * Created by John on 28.12.2016.
 */

public class AlphabeticalSort implements Comparator<Ship> {
    @Override
    public int compare(Ship o1, Ship o2) {
        return ((o1.toString().compareTo(o2.toString())));
    }

}
