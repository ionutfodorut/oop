import ship.contents.*;

/**
 * Created by John on 27.12.2016.
 */
public class App {

    public static void main(String[] args) {

        ShipBay shipBay1 = new ShipBay();

        Ship ship0 = new CargoShip("Akadia");
        Ship ship1 = new CargoShip("Arcadia");
        Ship ship2 = new PassengerShip("Gioconda");
        CargoCompartment compartment1 = new CargoCompartment("123432");
        PassengerCompartment compartment2 = new PassengerCompartment("323131");
        CargoCompartment compartment3 = new CargoCompartment("312313");
        PassengerCompartment compartment4 = new PassengerCompartment("76786876");

        ship1.addCompartment(compartment1);
        ship2.addCompartment(compartment2);
        ship1.addCompartment(compartment3);
        ship2.addCompartment(compartment4);

        Passenger passenger1 = new Passenger("John");
        Passenger passenger2 = new Passenger("George");
        Passenger passenger3 = new Passenger("Lily");
        compartment2.addPassenger(passenger1);
        compartment4.addPassenger(passenger2);
        compartment2.addPassenger(passenger3);

        CargoItem item1 = new CargoItem("floare", 20);
        CargoItem item2 = new CargoItem("copac", 200);
        compartment1.setCargoItem(item1);
        compartment3.setCargoItem(item2);
        compartment1.addCarriable();
        compartment1.addCarriable();
        compartment1.addCarriable();
        compartment3.addCarriable();
        compartment3.addCarriable();
        compartment3.addCarriable();
        compartment3.addCarriable();

        System.out.println(compartment1.getProfit());
        System.out.println(compartment2.getProfit());

        System.out.println(ship1.getProfit());
        System.out.println(ship2.getProfit());

        ship0.shipSummary();
        ship1.shipSummary();
        ship2.shipSummary();

        shipBay1.receiveShip(ship1);
        shipBay1.receiveShip(ship1);
        shipBay1.receiveShip(ship2);
        shipBay1.departShip(ship1);
        shipBay1.departShip(ship1);

        shipBay1.sortByName();

        shipBay1.receiveShip(ship1);
        shipBay1.receiveShip(ship0);

        shipBay1.sortByName();
        shipBay1.sortByProfit();

    }

}
