package javasmmr.zoowsome.controllers;

import javasmmr.zoowsome.models.Zoo;
import javasmmr.zoowsome.models.animals.Animal;
import javasmmr.zoowsome.models.animals.Tiger;
import javasmmr.zoowsome.models.employees.Caretaker;
import javasmmr.zoowsome.models.employees.Employee;
import javasmmr.zoowsome.repositories.AnimalRepository;
import javasmmr.zoowsome.services.Constants;
import javasmmr.zoowsome.services.factories.animalFactories.AnimalFactory;
import javasmmr.zoowsome.services.factories.employeeFactories.CaretakerFactory;
import javasmmr.zoowsome.services.factories.employeeFactories.EmployeeAbstractFactory;
import javasmmr.zoowsome.services.factories.employeeFactories.EmployeeFactory;
import javasmmr.zoowsome.services.factories.utilityFactories.ScientificNameFactory;
import javasmmr.zoowsome.services.factories.animalFactories.SpeciesFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;

/**
 * Created by John on 29.12.2016.
 */

public class MainController {
    public static void main(String[] args) throws Exception {
        AnimalFactory abstractFactory = new AnimalFactory();
        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory(Constants.Species.Mammals);
        Animal a1 = speciesFactory1.getAnimal(Constants.Animals.Mammals.Cow);

        Zoo clujZoo = new Zoo();
        clujZoo.populate(100);

        for (Animal a : clujZoo.getAnimalsInside()) {
            System.out.printf("We have an animal named %s with %d legs!\n", a.getName(), a.getNrOfLegs());
        }

        EmployeeAbstractFactory employeeAbstractFactory = new EmployeeAbstractFactory();
        EmployeeFactory caretakerFactory = employeeAbstractFactory.getEmployeeFactory(Constants.Employees.Caretakers.Caretaker);
        LinkedHashSet<Caretaker> caretakerSet = new LinkedHashSet<Caretaker>();
        for (int i = 0; i < 25; i++) {
            Employee c = caretakerFactory.getEmployee();
            caretakerSet.add((Caretaker)c);
            System.out.println(c.getName() + " employed at the Zoo with " + ((Caretaker) c).getWorkingHours() +
            " working hours");
        }
        for (Caretaker c : caretakerSet) {
            for (Animal a : clujZoo.getAnimalsInside()) {
                if ((!(c.isDead())) && (!(a.isTakenCareOf()))){
                    String result = c.takeCareOf(a);
                    if (result.equals(Constants.Employees.Caretakers.TCO_KILLED)){
                        c.setIsDead(true);
                        System.out.println(c.getName() + " has died in a fight with " + a.getName());
                    } else if (result.equals(Constants.Employees.Caretakers.TCO_NO_TIME)){
                        System.out.println(c.getName() + " hasn't got enough working hours for " + a.getName());
                    } else {
                        a.setTakenCareOf(true);
                        System.out.println(c.getName() + " has taken care of " + a.getName() + " for " + a.getMaintenanceCost() + " hours.");
                    }
                }
            }
        }

        for (Animal a : clujZoo.getAnimalsInside()){
            if (a.isTakenCareOf()){
                System.out.println(a.getName() + " of work cost " + a.getMaintenanceCost() + " has been taken care of.");
            } else {
                System.out.println(a.getName() + " has not been taken care of.");
            }
        }
        Tiger tiger = new Tiger();
        System.out.println(tiger.getPredisposition());
        AnimalRepository animalRepository = new AnimalRepository();
        ArrayList<Animal> animals = new ArrayList<Animal>(clujZoo.getAnimalsInside());
        animalRepository.save(animals);
    }
}
