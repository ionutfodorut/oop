package javasmmr.zoowsome.models.interfaces;

import javasmmr.zoowsome.models.animals.Animal;

/**
 * Created by John on 03.01.2017.
 */
public interface Caretaker_I {
    public String takeCareOf(Animal animal);
}
