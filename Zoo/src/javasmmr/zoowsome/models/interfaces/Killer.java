package javasmmr.zoowsome.models.interfaces;

/**
 * Created by John on 29.12.2016.
 */
public interface Killer {
    boolean kill();
    public double getPredisposition();
}
