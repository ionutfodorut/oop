package javasmmr.zoowsome.models;
import javasmmr.zoowsome.models.animals.*;
import javasmmr.zoowsome.services.factories.animalFactories.AnimalFactory;
import java.util.LinkedHashSet;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by John on 29.12.2016.
 */
public class Zoo {

    private LinkedHashSet<Animal> animalsInside;

    public Zoo() {
        this.animalsInside = new LinkedHashSet<Animal>();
    }

    private static final String[] taxonomicClass = {"Mammals", "Reptiles", "Birds", "Aquatics", "Insects"};
    private static final String[][] taxonomicSpecies = {{"Cow", "Tiger", "Monkey"},
                                           {"Crocodile", "Snake", "Turle"},
                                           {"Pelican", "Vulture", "WildDuck"},
                                           {"Frog", "Tuna", "Shark"},
                                           {"Butterfly", "Cockroach", "Spider"}};

    public void populate(int population) throws Exception {
        while (population > 0) {
                int randomNum1 = ThreadLocalRandom.current().nextInt(0, taxonomicClass.length);
                int randomNum2 = ThreadLocalRandom.current().nextInt(0, taxonomicSpecies[randomNum1].length - 1);
                AnimalFactory animalFactory = new AnimalFactory();
                animalsInside.add(animalFactory.getSpeciesFactory(taxonomicClass[randomNum1])
                             .getAnimal(taxonomicSpecies[randomNum1][randomNum2]));
                population--;
        }
    }

    public LinkedHashSet<Animal> getAnimalsInside() {
        return animalsInside;
    }
}

