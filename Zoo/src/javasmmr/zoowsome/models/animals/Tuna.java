package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;
import javasmmr.zoowsome.services.WaterType;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Tuna extends Aquatic {
    public Tuna() {
        super(1.3, 0.0);
        nrOfLegs = 0;
        //name = "Tony";
        avgSwimDepth = 2000;
        waterType = WaterType.Saltwater;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Aquatics.Tuna);
    }
}
