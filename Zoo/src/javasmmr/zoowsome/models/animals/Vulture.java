package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Vulture extends Bird {
    public Vulture() {
        super(6.0, 0.38);
        nrOfLegs = 2;
        //name = "Ken";
        migrates = Boolean.TRUE;
        avgFlightAltitude = 11500;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Birds.Vulture);
    }
}
