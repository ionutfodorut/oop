package javasmmr.zoowsome.models.animals;

import org.w3c.dom.Element;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public abstract class Insect extends Animal{
    protected Boolean canFly;
    protected Boolean isDangerous;
    protected Insect(double v, double v1) { super(v, v1); }
    public Boolean getCanFly() {
        return canFly;
    }
    public void setCanFly(Boolean canFly) {
        this.canFly = canFly;
    }
    public Boolean getDangerous() {
        return isDangerous;
    }
    public void setDangerous(Boolean dangerous) {
        isDangerous = dangerous;
    }
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, "canFly", String.valueOf(getCanFly()));
        createNode(eventWriter, "isDangerous", String.valueOf(getDangerous()));
    }
    public void decodeFromXml(Element element) {
        setCanFly(Boolean.valueOf(element.getElementsByTagName("canFly").item(0).getTextContent()));
        setDangerous(Boolean.valueOf(element.getElementsByTagName("isDangerous").item(0).getTextContent()));
    }
}
