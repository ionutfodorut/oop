package javasmmr.zoowsome.models.animals;
import javasmmr.zoowsome.models.interfaces.Killer;
import javasmmr.zoowsome.models.interfaces.XML_Parsable;
import javasmmr.zoowsome.services.factories.utilityFactories.ScientificNameFactory;

//import javax.xml.bind.Element;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public abstract class Animal implements Killer, XML_Parsable {
    protected final Double maintenanceCost;
    protected final Double dangerPerc;
    protected Boolean takenCareOf = false;
    protected int nrOfLegs;
    private ScientificNameFactory nameFactory = new ScientificNameFactory();
    protected String name = nameFactory.generateName();
    protected Animal(double maintenanceCost, double dangerPerc) {
        this.maintenanceCost = maintenanceCost;
        this.dangerPerc = dangerPerc;
    }
    public boolean isTakenCareOf() { return takenCareOf; }
    public void setTakenCareOf(boolean takenCareOf) { this.takenCareOf = takenCareOf; }
    public int getNrOfLegs() {
        return nrOfLegs;
    }
    public void setNrOfLegs(int nrOfLegs) {
        this.nrOfLegs = nrOfLegs;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Double getMaintenanceCost() { return maintenanceCost;}
    public boolean kill() {
        double i = Math.random();
        return (i > this.dangerPerc + getPredisposition());
    }
    public double getPredisposition(){
        return 0;
    }
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        createNode(eventWriter, "nrOfLegs", String.valueOf(this.nrOfLegs));
        createNode(eventWriter, "name", String.valueOf(this.name));
        createNode(eventWriter, "maintenanceCost", String.valueOf(this.maintenanceCost));
        createNode(eventWriter, "dangerPerc", String.valueOf(this.dangerPerc));
        createNode(eventWriter, "takenCareOf", String.valueOf(this.takenCareOf));
    }
    public void decodeFromXml(Element element) {
        setNrOfLegs(Integer.valueOf(element.getElementsByTagName("nrOfLegs").item(0).getTextContent()));
        setName(element.getElementsByTagName("name").item(0).getTextContent());
        //setMaintenanceCost(Double.valueOf(element.getElementsByTagName("maintenanceCost").item(0).getTextContent()));
        //setDangerPerc(Double.valueOf(element.getElementsByTagName("dangerPerc").item(0).getTextContent()));
        setTakenCareOf(Boolean.valueOf(element.getElementsByTagName("takenCareOf").item(0).getTextContent()));
    }
}
