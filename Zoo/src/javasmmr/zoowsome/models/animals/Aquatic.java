package javasmmr.zoowsome.models.animals;
import javasmmr.zoowsome.services.WaterType;
import org.w3c.dom.Element;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public abstract class Aquatic extends Animal {
    protected int avgSwimDepth;
    protected WaterType waterType;
    protected Aquatic(double v, double v1) { super(v, v1); }
    public int getAvgSwimDepth() {
        return avgSwimDepth;
    }
    public void setAvgSwimDepth(int avgSwimDepth) {
        this.avgSwimDepth = avgSwimDepth;
    }
    public WaterType getWaterType() {
        return waterType;
    }
    public void setWaterType(WaterType waterType) {
        this.waterType = waterType;
    }
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, "avgSwimDepth", String.valueOf(getAvgSwimDepth()));
        createNode(eventWriter, "waterType", String.valueOf(getWaterType()));
    }
    public void decodeFromXml(Element element) {
        setAvgSwimDepth(Integer.valueOf(element.getElementsByTagName("avgSwimDepth").item(0).getTextContent()));
        setWaterType(WaterType.valueOf(element.getElementsByTagName("waterType").item(0).getTextContent()));
    }
}
