package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Monkey extends Mammal{
    public Monkey() {
        super(5.0, 0.2);
        nrOfLegs = 2;
        //name = "Henni";
        normalBodyTemp = 35;
        percBodyHair = 100;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Mammals.Monkey);
    }
}
