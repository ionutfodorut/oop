package javasmmr.zoowsome.models.animals;

import org.w3c.dom.Element;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public abstract class Bird extends Animal {
    protected Boolean migrates;
    protected int avgFlightAltitude;
    protected Bird(double v, double v1) {
        super(v, v1);
    }
    public Boolean getMigrates() {
        return migrates;
    }
    public void setMigrates(Boolean migrates) {
        this.migrates = migrates;
    }
    public int getAvgFlightAltitude() {
        return avgFlightAltitude;
    }
    public void setAvgFlightAltitude(int avgFlightAltitude) {
        this.avgFlightAltitude = avgFlightAltitude;
    }
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, "migrates", String.valueOf(getMigrates()));
        createNode(eventWriter, "AvgFlightAltitude", String.valueOf(getAvgFlightAltitude()));
    }
    public void decodeFromXml(Element element) {
        setMigrates(Boolean.valueOf(element.getElementsByTagName("migrates").item(0).getTextContent()));
        setAvgFlightAltitude(Integer.valueOf(element.getElementsByTagName("avgFlightAltitude").item(0).getTextContent()));
    }
}
