package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import javax.xml.bind.Element;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Crocodile extends Reptile {
    public Crocodile() {
        super(6.5, 0.7);
        nrOfLegs = 4;
        //name = "James";
        laysEggs = Boolean.TRUE;
    }
    public double getPredisposition(){
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String formatedDate = ft.format(date);
        String hour = String.valueOf(formatedDate.charAt(19)) + String.valueOf(formatedDate.charAt(20));
        if (hour.equals("01")){
            return 0.12;
        }
        return 0;
    }

    @Override
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Reptiles.Crocodile);
    }
}
