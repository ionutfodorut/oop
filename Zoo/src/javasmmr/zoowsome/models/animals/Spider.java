package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Spider extends Insect {
    public Spider() {
        super(5.3, 0.65);
        nrOfLegs  = 8;
        //name = "Jasper";
        canFly = Boolean.FALSE;
        isDangerous = Boolean.TRUE;
    }
    @Override
    public double getPredisposition(){
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String formatedDate = ft.format(date);
        String day = String.valueOf(formatedDate.charAt(11)) + String.valueOf(formatedDate.charAt(12));
        if (day.equals("01")){
            return 0.20;
        }
        return 0;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Insects.Spider);
    }
}
