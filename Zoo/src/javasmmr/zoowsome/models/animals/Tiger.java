package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Tiger extends Mammal {
    public Tiger() {
        super(8.9, 0.62);
        nrOfLegs = 4;
        //name = "Tommy";
        normalBodyTemp = 36;
        percBodyHair = 100;

    }
    @Override
    public double getPredisposition(){
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        String formatedDate = ft.format(date);
        String month = String.valueOf(formatedDate.charAt(8)) + String.valueOf(formatedDate.charAt(9));
        if (month.equals("01")){
            return 0.30;
        }
        return 0;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Mammals.Tiger);
    }
}
