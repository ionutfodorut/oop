package javasmmr.zoowsome.models.animals;

import org.w3c.dom.Element;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public abstract class Reptile extends Animal {
    protected Boolean laysEggs;
    public Reptile(double v, double v1) {
        super(v, v1);
    }
    public Boolean getLaysEggs() {
        return laysEggs;
    }
    public void setLaysEggs(Boolean laysEggs) {
        this.laysEggs = laysEggs;
    }
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, "laysEggs", String.valueOf(getLaysEggs()));
    }
    public void decodeFromXml(Element element) {
        setLaysEggs(Boolean.valueOf(element.getElementsByTagName("avgSwimDepth").item(0).getTextContent()));
    }
}
