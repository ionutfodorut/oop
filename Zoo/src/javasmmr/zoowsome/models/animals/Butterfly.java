package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import org.w3c.dom.Element;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Butterfly extends Insect {
    public Butterfly() {
        super(2.2, 0.0);
        nrOfLegs = 6;
        //name = "Casper";
        canFly = Boolean.TRUE;
        isDangerous = Boolean.FALSE;
    }

    @Override
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Insects.Butterfly);
    }
}
