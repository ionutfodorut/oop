package javasmmr.zoowsome.models.animals;
import javasmmr.zoowsome.services.Constants;
import javasmmr.zoowsome.services.WaterType;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Shark extends Aquatic {
    public Shark() {
        super(6.0, 0.6);
        nrOfLegs = 0;
        //name = "Bob";
        avgSwimDepth = 2500;
        waterType = WaterType.Saltwater;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Aquatics.Shark);
    }
}
