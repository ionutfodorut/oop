package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Cow extends Mammal {
    public Cow() {
        super(4.6, 0.1);
        nrOfLegs = 4;
        //name = "Joiana";
        normalBodyTemp = 36;
        percBodyHair = 100;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Mammals.Cow);
    }
}
