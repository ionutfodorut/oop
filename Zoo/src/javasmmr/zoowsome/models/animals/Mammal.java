package javasmmr.zoowsome.models.animals;

import org.w3c.dom.Element;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public abstract class Mammal extends Animal {
    protected float normalBodyTemp;
    protected float percBodyHair;
    protected Mammal(double v, double v1) {
        super(v, v1);
    }
    public float getNormalBodyTemp() {
        return normalBodyTemp;
    }
    public void setNormalBodyTemp(float normalBodyTemp) {
        this.normalBodyTemp = normalBodyTemp;
    }
    public float getPercBodyHair() {
        return percBodyHair;
    }
    public void setPercBodyHair(float percBodyHair) { this.percBodyHair = percBodyHair; }
    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, "normalBodyTemp", String.valueOf(getNormalBodyTemp()));
        createNode(eventWriter, "percBodyHair", String.valueOf(getPercBodyHair()));
    }
    public void decodeFromXml(Element element) {
        setNormalBodyTemp(Float.valueOf(element.getElementsByTagName("normalBodyTemp").item(0).getTextContent()));
        setPercBodyHair(Float.valueOf(element.getElementsByTagName("percBodyHair").item(0).getTextContent()));
    }
}
