package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Turtle extends Reptile {
    public Turtle() {
        super(3.2, 0.0);
        nrOfLegs = 4;
        //name = "Squirtle";
        laysEggs = Boolean.TRUE;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Reptiles.Turtle);
    }
}
