package javasmmr.zoowsome.models.animals;

import javasmmr.zoowsome.services.Constants;
import javasmmr.zoowsome.services.WaterType;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import static javasmmr.zoowsome.repositories.AnimalRepository.createNode;

/**
 * Created by John on 28.12.2016.
 */
public class Frog extends Aquatic {
    public Frog() {
        super(2.3, 0.3);
        nrOfLegs = 4;
        //name = "Winston";
        avgSwimDepth = 2;
        waterType = WaterType.Freshwater;
    }

    public void encodeToXml(XMLEventWriter eventWriter) throws XMLStreamException {
        super.encodeToXml(eventWriter);
        createNode(eventWriter, Constants.XML_TAGS.DISCRIMINANT, Constants.Animals.Aquatics.Frog);
    }
}
