package javasmmr.zoowsome.models.employees;
import javasmmr.zoowsome.services.exceptions.NotUniqueException;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by John on 03.01.2017.
 */
public abstract class Employee {
    protected String name;
    protected /*final*/ Long id = Long.valueOf(0); //Should this be final?
    protected BigDecimal salary;
    protected Boolean isDead = false;
    private static Set<Long> assignedIds = new HashSet<Long>();

    public Employee(String name, BigDecimal salary, Long id) throws NotUniqueException {
        this.name = name;
        this.salary = salary;
        setId(id);
        //this.id = null;
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public void setId(Long id) throws NotUniqueException {
        if (!(this.id.equals(id))) {
            if (assignedIds.contains(id)) {
                throw new NotUniqueException();
            }
            assignedIds.add(id);
            this.id = id;
        }
    }
    public Long getId() { return id; }
    public BigDecimal getSalary() { return salary; }
    public void setSalary(BigDecimal salary) { this.salary = salary; }
    public Boolean isDead() { return isDead; }
    public void setIsDead(Boolean dead) { isDead = dead; }
}
