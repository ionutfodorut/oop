package javasmmr.zoowsome.models.employees;

import javasmmr.zoowsome.models.interfaces.Caretaker_I;
import javasmmr.zoowsome.models.animals.Animal;
import javasmmr.zoowsome.services.Constants;
import javasmmr.zoowsome.services.exceptions.NotUniqueException;

import java.math.BigDecimal;

/**
 * Created by John on 03.01.2017.
 */
public class Caretaker extends Employee implements Caretaker_I {
    protected Double workingHours;
    public Caretaker(String name, BigDecimal salary, Long id, double workingHours) throws NotUniqueException {
        super(name, salary, id);
        this.workingHours = workingHours;
    }
    public Double getWorkingHours() { return workingHours; }
    public void setWorkingHours(Double workingHours) { this.workingHours = workingHours; }

    @Override
    public String takeCareOf(Animal animal) {
        if (animal.kill()) {
            return Constants.Employees.Caretakers.TCO_KILLED;
        }
        if (this.workingHours < animal.getMaintenanceCost()){
            return Constants.Employees.Caretakers.TCO_NO_TIME;
        }
        animal.setTakenCareOf(true);
        this.workingHours -= animal.getMaintenanceCost();
    return Constants.Employees.Caretakers.TCO_SUCCESS;
    }
}
