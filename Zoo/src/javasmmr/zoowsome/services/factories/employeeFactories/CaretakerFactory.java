package javasmmr.zoowsome.services.factories.employeeFactories;

import javasmmr.zoowsome.models.employees.Caretaker;
import javasmmr.zoowsome.models.employees.Employee;
import javasmmr.zoowsome.services.factories.utilityFactories.PersonNameFactory;
import javasmmr.zoowsome.services.factories.utilityFactories.UniqueIdFactory;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;

/**
 * Created by John on 03.01.2017.
 */
public class CaretakerFactory extends EmployeeFactory {
    @Override
    public Employee getEmployee() throws Exception {
        PersonNameFactory personNameFactory = new PersonNameFactory();
        return new Caretaker(personNameFactory.generateName(), BigDecimal.valueOf(2500), UniqueIdFactory.generateUniqueId(), ThreadLocalRandom.current().nextInt(8, 32));
    }
}
