package javasmmr.zoowsome.services.factories.employeeFactories;

import javasmmr.zoowsome.services.Constants;

/**
 * Created by John on 03.01.2017.
 */
public class EmployeeAbstractFactory {
    public EmployeeFactory getEmployeeFactory(String type) throws Exception {
        if (Constants.Employees.Caretakers.Caretaker.equals(type)) {
            return new CaretakerFactory();
        } else {
            throw new Exception("Invalid employee type exception!");
        }
    }
}
