package javasmmr.zoowsome.services.factories.employeeFactories;

import javasmmr.zoowsome.models.employees.Employee;

/**
 * Created by John on 03.01.2017.
 */
public abstract class EmployeeFactory {
    public abstract Employee getEmployee() throws Exception;
}
