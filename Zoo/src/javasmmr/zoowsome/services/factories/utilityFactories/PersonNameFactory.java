package javasmmr.zoowsome.services.factories.utilityFactories;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by John on 03.01.2017.
 */
public class PersonNameFactory {
    public String generateName(){
        String[] firstNames = {"John", "Lisa", "Mary", "Peter", "Paul", "Amanda", "Richard", "Elisabeth"};
        String[] lastNames = {"Popescu", "DaVinci", "Iohannis", "Dragnea", "Melescanu", "Trump", "Obama"};
        int randomNum1 = ThreadLocalRandom.current().nextInt(0, firstNames.length);
        int randomNum2 = ThreadLocalRandom.current().nextInt(0, lastNames.length);
        return firstNames[randomNum1] + " " + lastNames[randomNum2];
    }
}
