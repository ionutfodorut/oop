package javasmmr.zoowsome.services.factories.utilityFactories;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by John on 29.12.2016.
 */
public class ScientificNameFactory {

    public String generateName() {
        String[][] syllables = {{"Dipto", "Ai", "Lyn", "Bra"},
                                {"li", "gu", "pho", "di"},
                                {"dae ", "tes ", "zoa ", "osa "},
                                {"Pro", "Bu", "Sum", "Tetra"},
                                {"ta", "la", "on", "rus"}};
        String name = "";
        for (int i = 0; i < syllables.length; i++){
            int randomNum = ThreadLocalRandom.current().nextInt(0, syllables[i].length);
            name += syllables[i][randomNum];
        }
        return name;
    }

}
