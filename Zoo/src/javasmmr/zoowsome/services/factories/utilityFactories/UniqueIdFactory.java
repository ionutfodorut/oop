package javasmmr.zoowsome.services.factories.utilityFactories;

import java.math.BigDecimal;
import static java.lang.Math.*;
import static oracle.jrockit.jfr.events.Bits.longValue;

/**
 * Created by John on 03.01.2017.
 */
public class UniqueIdFactory {
    public static Long generateUniqueId(){
        BigDecimal a = new BigDecimal("1000000000000");
        BigDecimal b = new BigDecimal("9000000000000");
        BigDecimal c = new BigDecimal(random());
        BigDecimal n = a.add(b.multiply(c));
        return longValue(n);
    }
}
