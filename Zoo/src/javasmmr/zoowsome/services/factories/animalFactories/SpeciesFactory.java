package javasmmr.zoowsome.services.factories.animalFactories;

import javasmmr.zoowsome.models.animals.Animal;

/**
 * Created by John on 28.12.2016.
 */
public abstract class SpeciesFactory {
    public abstract Animal getAnimal(String type) throws Exception;
}