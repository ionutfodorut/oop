package javasmmr.zoowsome.services.factories.animalFactories;

import javasmmr.zoowsome.models.animals.Animal;
import javasmmr.zoowsome.models.animals.Cow;
import javasmmr.zoowsome.models.animals.Monkey;
import javasmmr.zoowsome.models.animals.Tiger;
import javasmmr.zoowsome.services.Constants;

/**
 * Created by John on 28.12.2016.
 */
public class MammalFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) throws Exception {
        if (Constants.Animals.Mammals.Monkey.equals(type)){
            return new Monkey();
        } else if (Constants.Animals.Mammals.Cow.equals(type)){
            return new Cow();
        } else if (Constants.Animals.Mammals.Tiger.equals(type)){
            return new Tiger();
        } else {
            throw new Exception("Invalid animal exception!");
        }
    }
}
