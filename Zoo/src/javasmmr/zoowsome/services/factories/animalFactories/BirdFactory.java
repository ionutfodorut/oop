package javasmmr.zoowsome.services.factories.animalFactories;

import javasmmr.zoowsome.models.animals.Animal;
import javasmmr.zoowsome.models.animals.Pelican;
import javasmmr.zoowsome.models.animals.Vulture;
import javasmmr.zoowsome.models.animals.WildDuck;
import javasmmr.zoowsome.services.Constants;

public class BirdFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) throws Exception {
        if (Constants.Animals.Birds.Pelican.equals(type)){
            return new Pelican();
        } else if (Constants.Animals.Birds.Vulture.equals(type)){
            return new Vulture();
        } else if (Constants.Animals.Birds.WildDuck.equals(type)){
            return new WildDuck();
        } else {
            throw new Exception("Invalid animal exception!");
        }
    }
}
