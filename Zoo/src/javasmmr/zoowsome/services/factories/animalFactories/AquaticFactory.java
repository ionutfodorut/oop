package javasmmr.zoowsome.services.factories.animalFactories;

import javasmmr.zoowsome.models.animals.Animal;
import javasmmr.zoowsome.models.animals.Frog;
import javasmmr.zoowsome.models.animals.Shark;
import javasmmr.zoowsome.models.animals.Tuna;
import javasmmr.zoowsome.services.Constants;

/**
 * Created by John on 29.12.2016.
 */

    public class AquaticFactory extends SpeciesFactory {

        @Override
        public Animal getAnimal(String type) throws Exception {
            if (Constants.Animals.Aquatics.Frog.equals(type)){
                return new Frog();
            } else if (Constants.Animals.Aquatics.Shark.equals(type)){
                return new Shark();
            } else if (Constants.Animals.Aquatics.Tuna.equals(type)){
                return new Tuna();
            } else {
                throw new Exception("Invalid animal exception!");
            }
        }
    }
