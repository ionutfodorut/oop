package javasmmr.zoowsome.services;

public enum WaterType {
    Saltwater, Freshwater
}
