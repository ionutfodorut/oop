package javasmmr.zoowsome.services.exceptions;

/**
 * Created by John on 03.01.2017.
 */
public class NotUniqueException extends Exception {
    public NotUniqueException() {
        System.out.println("ID already in list.");
    }
}
