package javasmmr.zoowsome.services;

/**
 * Created by John on 28.12.2016.
 */
public final class Constants {

    public static final class Species {
        public static final String Mammals = "Mammals";
        public static final String Reptiles = "Reptiles";
        public static final String Birds = "Birds";
        public static final String Aquatics = "Aquatics";
        public static final String Insects = "Insects";
    }

    public static final class Animals {
        public static final class Mammals {
            public static final String Tiger = "Tiger";
            public static final String Cow = "Cow";
            public static final String Monkey = "Monkey";
        }

        public static final class Reptiles {
            public static final String Snake = "Snake";
            public static final String Crocodile = "Crocodile";
            public static final String Turtle = "Turtle";
        }

        public static final class Birds {
            public static final String Pelican = "Pelican";
            public static final String Vulture = "Vulture";
            public static final String WildDuck = "WildDuck";
        }

        public static final class Insects {
            public static final String Butterfly = "Butterfly";
            public static final String Spider = "Spider";
            public static final String Cockroach = "Cockroach";
        }

        public static final class Aquatics {
            public static final String Frog = "Frog";
            public static final String Tuna = "Tuna";
            public static final String Shark = "Shark";
        }

    }

    public static final class Employees {

        public static final class Caretakers {
                public static final String Caretaker = "Caretaker";
                public static final String TCO_SUCCESS = "SUCCESS";
                public static final String TCO_KILLED = "KILLED";
                public static final String TCO_NO_TIME = "NO_TIME";
        }
    }

    public static final class XML_TAGS {
        public static final String ANIMAL = "ANIMAL";
        public static final String DISCRIMINANT = "DISCRIMINANT";
    }

}
